CC = gcc

NAME = libft.a

SRCS = srcs/lesfichiers.c

HEADER_DIR = includes/lesfichiers.h

CFLAGS = -c -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	$(CC) $(CFLAGS) $(SRCS) -I $(HEADER_DIR)
	ar rc $(NAME) lesfichiers.o
	ranlib $(NAME)

clean:
	/bin/rm -f lesfichiers.o
fclean:
	/bin/rm -f $(NAME)
re:
	fclean all
